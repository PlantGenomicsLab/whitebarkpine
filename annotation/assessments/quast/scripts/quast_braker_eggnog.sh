#!/bin/bash
#SBATCH --job-name=quast_braker_eggnog
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load quast
quast.py -v
#quast.py -o braker_eggnog_filt /core/labs/Wegrzyn/whitebarkpine/stringtie/braker_eggnog/braker_genes_eggnog_filtered.cds 
quast.py -o braker_eggnog_60 /core/labs/Wegrzyn/whitebarkpine/stringtie/going_insane/braker_eggnog_60.cds
echo -e "\nEnd time:"
date

