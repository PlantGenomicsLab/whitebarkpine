#!/bin/bash
#SBATCH --job-name=quast_st2_filt
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date


module load quast
quast.py -v
quast.py -o st2_filt /core/labs/Wegrzyn/whitebarkpine/stringtie/going_insane/stringtie2_hybrid_allshort_transcripts.fa.transdecoder.cds


echo -e "\nEnd time:"
date

