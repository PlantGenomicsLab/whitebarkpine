All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    combined_stringtie_eggnog.cds
# contigs (>= 0 bp)         271318                       
# contigs (>= 1000 bp)      71765                        
# contigs (>= 5000 bp)      620                          
# contigs (>= 10000 bp)     23                           
# contigs (>= 25000 bp)     0                            
# contigs (>= 50000 bp)     0                            
Total length (>= 0 bp)      226382259                    
Total length (>= 1000 bp)   121404042                    
Total length (>= 5000 bp)   3889050                      
Total length (>= 10000 bp)  283530                       
Total length (>= 25000 bp)  0                            
Total length (>= 50000 bp)  0                            
# contigs                   166574                       
Largest contig              16935                        
Total length                188317989                    
GC (%)                      44.13                        
N50                         1260                         
N90                         621                          
auN                         1596.5                       
L50                         47417                        
L90                         132765                       
# N's per 100 kbp           0.56                         
