All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    stringtie2_braker_60_redo.cds
# contigs (>= 0 bp)         262730                       
# contigs (>= 1000 bp)      66961                        
# contigs (>= 5000 bp)      558                          
# contigs (>= 10000 bp)     23                           
# contigs (>= 25000 bp)     0                            
# contigs (>= 50000 bp)     0                            
Total length (>= 0 bp)      215197919                    
Total length (>= 1000 bp)   112450836                    
Total length (>= 5000 bp)   3545421                      
Total length (>= 10000 bp)  285651                       
Total length (>= 25000 bp)  0                            
Total length (>= 50000 bp)  0                            
# contigs                   159550                       
Largest contig              16935                        
Total length                177718164                    
GC (%)                      44.11                        
N50                         1236                         
N90                         618                          
auN                         1570.0                       
L50                         45672                        
L90                         127521                       
# N's per 100 kbp           0.59                         
