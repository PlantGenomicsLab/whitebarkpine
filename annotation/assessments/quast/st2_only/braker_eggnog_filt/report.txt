All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    braker_genes_eggnog_filtered.cds
# contigs (>= 0 bp)         219474                          
# contigs (>= 1000 bp)      50991                           
# contigs (>= 5000 bp)      267                             
# contigs (>= 10000 bp)     8                               
# contigs (>= 25000 bp)     0                               
# contigs (>= 50000 bp)     0                               
Total length (>= 0 bp)      170279169                       
Total length (>= 1000 bp)   82784355                        
Total length (>= 5000 bp)   1639236                         
Total length (>= 10000 bp)  90687                           
Total length (>= 25000 bp)  0                               
Total length (>= 50000 bp)  0                               
# contigs                   129493                          
Largest contig              15351                           
Total length                137907522                       
GC (%)                      44.10                           
N50                         1164                            
N90                         603                             
auN                         1451.5                          
L50                         38145                           
L90                         104376                          
# N's per 100 kbp           0.77                            
