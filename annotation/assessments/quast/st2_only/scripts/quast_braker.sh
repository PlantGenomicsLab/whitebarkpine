#!/bin/bash
#SBATCH --job-name=quast_braker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load quast
quast.py -v
quast.py -o braker_only /core/labs/Wegrzyn/whitebarkpine/braker/braker/augustus.hints.codingseq



echo -e "\nEnd time:"
date

