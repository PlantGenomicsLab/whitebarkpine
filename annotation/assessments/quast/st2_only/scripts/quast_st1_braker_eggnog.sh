#!/bin/bash
#SBATCH --job-name=quast_st1_braker_eggnog
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load quast
quast.py -v
quast.py -o st1_braker_eggnog_60 /core/labs/Wegrzyn/whitebarkpine/stringtie/going_insane/stringtie_braker_eggnog_60.cds
quast.py -o st1_braker_eggnog /core/labs/Wegrzyn/whitebarkpine/stringtie/combined_stringtie_eggnog.cds
quast.py -o st1_braker_2line /core/labs/Wegrzyn/whitebarkpine/stringtie/combined_stringtie2line_eggnog.cds


echo -e "\nEnd time:"
date

