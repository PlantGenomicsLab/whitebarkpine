#!/bin/bash
#SBATCH --job-name=braker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

org=/core/labs/Wegrzyn/whitebarkpine
mkdir -p /core/labs/Wegrzyn/whitebarkpine/braker/tmp

module load python/3.6.3
module load biopython/1.70
module load perl/5.28.1
module load bamtools/2.5.1
module load blast/2.10.0
module load genomethreader/1.7.1

export AUGUSTUS_CONFIG_PATH=$HOME/personal_software/Augustus_3.3.3/config/
export AUGUSTUS_BIN_PATH=$HOME/personal_software/Augustus_3.3.3/bin
export PATH=$HOME/personal_software/BRAKER_2.1.5:$HOME/personal_software/BRAKER_2.1.5/scripts:$PATH
export TMPDIR=/core/labs/Wegrzyn/whitebarkpine/braker/tmp
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.5.1/bin
export BLAST_PATH=/isg/shared/apps/blast/ncbi-blast-2.10.0+/bin
export CDBTOOLS_PATH=$HOME/personal_software/cdbfasta
export SAMTOOLS_PATH=/isg/shared/apps/samtools/1.9/bin
export GENEMARK_PATH=/core/labs/Wegrzyn/annotationtool/software/gmes_linux_64

species='whitebarkpine'
genome=/core/labs/Wegrzyn/whitebarkpine/repeats/split_genome/splits/masked_files/masked_wbp_old_rm.fa

bam1=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR13823648.bam
bam2=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR4786281.bam
bam3=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR4786284.bam
bam4=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368574.bam
bam5=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368575.bam
bam6=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368576.bam
bam7=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368577.bam
bam8=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368578.bam
bam9=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368579.bam
bam10=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368580.bam
bam11=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368581.bam
bam12=/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/sorted_SRR5368582.bam

if [[ -d $AUGUSTUS_CONFIG_PATH/species/$species ]]; then rm -r $AUGUSTUS_CONFIG_PATH/species/$species; fi

#cp ~/local_gm_key ~/.gm_key
#cp ~/local_gm_key $homedir/.gm_key

braker.pl --cores 20 --genome="$genome" --species="$species" --bam="$bam1,$bam2,$bam3,$bam4,$bam5,$bam6,$bam7,$bam8,$bam9,$bam10,$bam11,$bam12" --softmasking 1 --gff3

echo -e "\nEnd time:"
date

