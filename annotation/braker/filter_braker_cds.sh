#!/bin/bash
#SBATCH --job-name=filter_braker_cds
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load seqtk
cds="/core/labs/Wegrzyn/whitebarkpine/wbp_annotation/braker/braker/augustus.hints.codingseq"
#seqtk subseq -l 60 /core/labs/Wegrzyn/whitebarkpine/braker/braker/augustus.hints.codingseq genes_w_egg_match_filt.txt > braker_eggnog_60.cds
seqtk subseq -l 400 $cds genes_w_egg_match_filt.txt > braker_eggnog_400.cds.fa
grep ">" braker_eggnog_400.cds.fa > braker_eggnog_400.txt 
sed -i "s/>//g" braker_eggnog_400.txt 
seqtk subseq augustus.hints.aa braker_eggnog_400.txt > braker_eggnog_400.pep.fa 




echo -e "\nEnd time:"
date

