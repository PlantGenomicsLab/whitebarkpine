#!/bin/bash
#SBATCH --job-name=get_fa_from_gtf
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load gffread/0.12.1

org=/core/labs/Wegrzyn/whitebarkpine/data/

#gtf="lauras_trans_aligned.gtf"
gtf=stringtie_hybrid_allshort.gtf

#extract transcript sequences from annotation file
gffread -w stringtie2_hybrid_allshort_transcripts.fa -g $org/wbp_3kb_filtered.fa $gtf


echo -e "\nEnd time:"
date

