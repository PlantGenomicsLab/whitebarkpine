#!/bin/bash
#SBATCH --job-name=stringtie_hybrid
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 18 
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load stringtie/2.2.1
module load samtools

stringtie -o stringtie_hybrid_allshort.gtf -l wbp_mix_allshort -p 18 --mix /core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/combined.bam /core/labs/Wegrzyn/WBP_DE/WBP/test/assemblies/test.ORP.fasta.transdecoder_dir/entap_outfiles/final_results/genotrans1.primaries.mapped.sorted.bam 


#/core/labs/Wegrzyn/whitebarkpine/hisat2/alignments/combined.bam

echo -e "\nEnd time:"
date

