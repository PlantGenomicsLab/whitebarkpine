#!/bin/bash
#SBATCH --job-name=stringtie
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname 
date

module load stringtie/2.2.1
stringtie -o SRR4786281_stringtie.gtf -l wbp -p 8 /core/labs/Wegrzyn/whitebarkpine/hisat2/sorted_SRR4786281.bam

date
