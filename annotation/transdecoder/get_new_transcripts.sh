#!/bin/bash
#SBATCH --job-name=get_new_transcripts
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load gffread/0.9.12
### **stringtie all short eggnog filtered**
st_gff="/core/labs/Wegrzyn/whitebarkpine/stringtie/transdecoder_genome/stringtie.transdecoder.genome.gff3"
st2_gff="/core/labs/Wegrzyn/whitebarkpine/stringtie/transdecoder_genome/stringtie.transdecoder2.genome.gff3"
genome="/core/labs/Wegrzyn/whitebarkpine/repeats/split_genome/splits/masked_files/double_masked_wbp.fa"

gffread -x stringtie_short.fa -g $genome $st_gff 
gffread -x stringtie_hybrid.fa -g $genome $st2_gff



echo -e "\nEnd time:"
date
