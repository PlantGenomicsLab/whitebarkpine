#!/bin/bash
#SBATCH --job-name=rename_transcripts
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date


#sed 's/MSTRG/PIAL/g' stringtie_short.pep > stringtie_renamed.pep.fa 
#sed 's/MSTRG/PIAL/g' stringtie_short.cds > stringtie_renamed.cds.fa 

sed 's/MSTRG/PIAL/g' /core/labs/Wegrzyn/whitebarkpine/stringtie/transdecoder_genome/entap_stringtie_short_plusuni/final_results/final_annotations_lvl0.tsv > whitebarkpine_entap_renamed.tsv 



echo -e "\nEnd time:"
date

