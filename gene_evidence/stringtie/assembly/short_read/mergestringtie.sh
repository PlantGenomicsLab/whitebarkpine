#!/bin/bash
#SBATCH --job-name=mergestringtie
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load stringtie/2.2.1
ls -1 *.gtf >> sample_assembly_gtf_list.txt
stringtie --merge -p 8 -o whitebark_stringtie_merged.gtf sample_assembly_gtf_list.txt  





echo -e "\nEnd time:"
date

