for i in SRR13823648 SRR5368574 SRR5368575 SRR5368576 SRR5368577 SRR5368578 SRR5368579 SRR5368580 SRR5368581 SRR5368582 SRR4786281 SRR4786284
do
echo '#!/bin/bash
#SBATCH --job-name=stringtie
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname 
date
'"
module load stringtie/2.2.1
stringtie -o ${i}_stringtie.gtf -l wbp -p 8 /core/labs/Wegrzyn/whitebarkpine/hisat2/sorted_${i}.bam

date" > stringtie_${i}_script.sh 
sbatch stringtie_${i}_script.sh 
done 