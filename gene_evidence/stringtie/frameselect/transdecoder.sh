#!/bin/bash
#SBATCH --job-name=transdecoder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load sqlite
module load hmmer/3.3.2
module load perl/5.24.0
module load TransDecoder/5.5.0
module load blast/2.11.0

source activate eggnog
filename=stringtie_merge_transcripts.fa
#TransDecoder.LongOrfs -t $filename
emapper.py -i $filename.transdecoder_dir/longest_orfs.pep -o eggnog.blastp -m diamond --cpu 16
TransDecoder.Predict -t $filename --retain_blastp_hits eggnog.blastp.emapper.hits




echo -e "\nEnd time:"
date

