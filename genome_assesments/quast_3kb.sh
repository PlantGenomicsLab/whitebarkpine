#!/bin/bash
#SBATCH --job-name=quast2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load quast/5.2.0

quast.py -t 12 -o quast3kb wbp_3kb_filtered.fa



echo -e "\nEnd time:"
date

