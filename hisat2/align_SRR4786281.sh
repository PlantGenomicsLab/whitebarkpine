#!/bin/bash
#SBATCH --job-name=hisatalign_SRR4786281
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load hisat2/2.2.1
module load samtools

mkdir -p alignments
hisat2 -p 12 -x wbp_3kb_filt --max-intronlen 2500000 -1 ../fastp/trimmed_SRR4786281_1.fastq.gz -2 ../fastp/trimmed_SRR4786281_2.fastq.gz -S SRR4786281.sam
samtools view -b -@ 12 SRR4786281.sam | samtools sort -o sorted_SRR4786281.bam -@ 12
rm SRR4786281.sam

date
