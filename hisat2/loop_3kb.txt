for i in SRR13823648 SRR5368574 SRR5368575 SRR5368576 SRR5368577 SRR5368578 SRR5368579 SRR5368580 SRR5368581 SRR5368582 SRR4786281 SRR4786284
do 
echo '#!/bin/bash
#SBATCH --job-name='"hisatalign_${i}"'
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date'"

module load hisat2
module load samtools

mkdir -p alignments
hisat2 -p 12 -x wbp_3kb_filt --max-intronlen 2500000 -1 ../fastp/trimmed_${i}_1.fastq.gz -2 ../fastp/trimmed_${i}_2.fastq.gz -S ${i}.sam
samtools view -b -@ 12 ${i}.sam | samtools sort -o sorted_${i}.bam -@ 12
rm ${i}.sam

date" > align_${i}.sh 
sbatch align_${i}.sh 
done 

