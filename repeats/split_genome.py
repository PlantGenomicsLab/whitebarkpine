#!/usr/bin/python

from Bio import SeqIO
import os

pieces=100

filename=open("/core/labs/Wegrzyn/whitebarkpine/data/wbp_3kb_filtered.fa", 'r')
seq_records1 = SeqIO.parse(filename, "fasta")
seq_records1 = list(seq_records1)
filename1="wbp_3kb_filtered.fa"
print(seq_records1)

# Calulate total number of bases

genome=0

for seq_record in seq_records1:
   genome+=len(seq_record.seq)

splice=genome/pieces  
j=0 
for i in range(pieces):
    c = filename1 + str(i+1) + ".fa"
    breaks=0
    with open(c,'w') as ijk:        
         while (breaks < splice) and j < len(seq_records1):
             ijk.write("%s%s\n%s\n" % (">",seq_records1[j].id, seq_records1[j].seq)) # Making the fasta file
             breaks+=len(seq_records1[j].seq)
             j+=1
    ijk.close()


